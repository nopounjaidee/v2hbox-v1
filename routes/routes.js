
module.exports = function(app)
{   
    const MS = require('../package/view/Maintenance_service');
    const RG = require('../package/view/Register');
    const CT = require('../package/view/Content');
    app.get('/maintenance', MS.Maintenance);
    app.post('/addkeycode', MS.Createkeycode );
    app.post('/version', MS.Checkversion );
    app.post('/register', RG.Registerboxtv );
    app.post('/checkuser',RG.Checkuser);
    app.post('/content_datasubtab',CT.Contentdatasubtab);
    app.post('/continue_watching',CT.Continuewatching);
    app.post('/menu_mainpage',CT.Memumain);
    app.post('/liverecommen',CT.Livetvall);
    app.post('/content_data',CT.Contentdata);
    app.post('/content',CT.Content);
    app.post('/content_name',CT.ContenpluslinkA);
    app.get('/content_name/:chname/:type',CT.ContenpluslinkB);

    app.use(function(req, res, next) {
        console.log("Link Part ไม่ถูกต้อง !! ");
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    app.use(function(err, req, res, next) {
        log.error("มีบางอย่างผิดพลาด !!! :", err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
};