var cluster = require('express-cluster');

cluster(function() {
    var express = require('./configs/config');
    var app = express();
    var server = app.listen(app.get('port'), function() {
      console.log('Express server listening on port ', server.address().port)
    });
  });