var sql = require('../connects/con_v2hdb');
var Services = require('../controllers/Controller_service');
var contents = {};
contents.Q_mainpagemenu = function(){
    return new Promise((resolve, reject) => {
        sql.query("SELECT maintab.id,maintab.name_th as name,maintab.img,mediadata.name as type ,maintab.language_id as lang FROM maintab INNER JOIN mediadata ON mediadata.id = maintab.mediadata_id ORDER BY `maintab`.`sort_num` ASC",(err, result) => {
            if(err)throw err;
            resolve(result)
        });
    });
};
contents.Q_livetvall = function(){
    return new Promise((resolve, reject) => {
        // sql.query("SELECT contentdata.id,contentdata.logo1 as logo,contentdata.logo2,contentdata.name_th as ch_name ,contentdata.link_full as link ,contentdata.chanel_num as ch_num FROM maintab INNER JOIN subtab ON subtab.maintab_id = maintab.id INNER JOIN content_subtab ON content_subtab.subtab_id = subtab.id INNER JOIN contentdata ON contentdata.id = content_subtab.contentdata_id WHERE subtab.id = '20' AND contentdata.deploy_status = '1' ORDER BY content_subtab.num_sort ASC",(err, result) => {
            sql.query("SELECT contentdata.id,contentdata.logo1 as logo,contentdata.logo2,contentdata.name_th as ch_name ,contentdata.link_full as link ,contentdata.chanel_num as ch_num FROM `contentdata` WHERE mediadata_id = '1' ORDER BY `contentdata`.`chanel_num` ASC",(err, result) => {
            if(err)throw err;
            resolve(result)
        });
    });
};
contents.Q_contentdata = function(id_mainpage,usergen,type){
    return new Promise(async(resolve, reject) => {
        var arrylive = []
        var arryvod = []
        let subtablist = await contents.Q_subtabfrommainpage(id_mainpage);
        if(id_mainpage == "1"){
            for (let i = 0; i < subtablist.length; i++) {
                let contentlist = await contents.Q_contentfromsubtab_live(subtablist[i].id);
                if(contentlist.length > 0){
                    switch (subtablist[i].type) {
                        case "A": 
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[]};
                                contentlist.forEach(function(item) {
                                     arr.item.push(item);
                                });
                                arrylive.push(arr)
                            break;
                        case "B":
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[]};
                                contentlist.forEach(function(item) {
                                     arr.item.push(item);
                                });
                                arrylive.push(arr)
                            break;
                        case "C":
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[{logo:subtablist[i].img,logo2:subtablist[i].img,ch_name:"firstbox",link:"null",ch_num:"null"}]};
                                contentlist.forEach(function(item) {
                                     arr.item.push(item);
                                });
                                arrylive.push(arr)
                            break;
                        case "D":
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[{logo:subtablist[i].img,logo2:subtablist[i].img,ch_name:"SeeAll",id:subtablist[i].id,type:type,link:"null",ch_num:"null"}]};
                                arrylive.push(arr)
                            break;  
                    
                        default:
                            break;
                    }
                }else{}
            }
            resolve(arrylive);
        }else{
            for (let i = 0; i < subtablist.length; i++) {
                let contentlist = await contents.Q_contentfromsubtab_vod(subtablist[i].id,subtablist[i].name,usergen,type);
                
                
                if(contentlist.length > 0){
                    // console.log(subtablist[i].name)
                    // console.log(contentlist)
                    switch (subtablist[i].type) {
                        case "A": 
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[]};
                                contentlist.forEach(function(item) {
                                     arr.item.push(item);
    
                                });
                                arryvod.push(arr)
                            break;
                        case "B":
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[]};
                                contentlist.forEach(function(item) {
                                     arr.item.push(item);
                                });
                                arryvod.push(arr)
                            break;
                        case "C":
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[{logo:subtablist[i].img,baner_logo:subtablist[i].img,ch_name:"firstbox",id:0,type:"null",detail:"null"}]};
                                contentlist.forEach(function(item) {
                                     arr.item.push(item);
                                });
                                arryvod.push(arr)
                            break;
                        case "D":
                                var arr = {subtitle:subtablist[i].name,type:subtablist[i].type,item:[{logo:subtablist[i].img,logo2:subtablist[i].img,ch_name:"SeeAll",id:subtablist[i].id,type:type,link:"null",ch_num:"null"}]};
                                arryvod.push(arr)
                            break;  
                    
                        default:
                            break;
                    }

                }else{}
            }
            resolve(arryvod);
        }
        // console.log(subtablist)
        // resolve(subtablist);
    });
};
contents.Q_contentsubtab = function(idsubtab,usergen){
    return new Promise((resolve, reject) => {
        var arry = []
        sql.query("SELECT contentdata.logo1 as logo,contentdata.logo2 ,contentdata.name_th as ch_name,contentdata.chanel_num as ch_num,contentdata.id,contentdata.link_full as link,mediadata.name as type ,contentdata.detail FROM maintab INNER JOIN subtab ON subtab.maintab_id = maintab.id INNER JOIN content_subtab ON content_subtab.subtab_id = subtab.id INNER JOIN contentdata ON contentdata.id = content_subtab.contentdata_id INNER JOIN mediadata ON mediadata.id = contentdata.mediadata_id WHERE subtab.id = '"+idsubtab+"' AND contentdata.deploy_status = '1' ORDER BY content_subtab.num_sort ASC",async(err, result) => {
            if(err)throw err;
            if(result[0].type == "live"){
                for (let i = 0; i < result.length; i++) {

                    arry.push({id:result[i].id,logo:result[i].logo, logo2:result[i].logo2, ch_name:result[i].ch_name,  link:result[i].link,type:result[i].type,
                        ch_num:result[i].ch_num});
                }
                resolve(arry);

            }else{
                for (let i = 0; i < result.length; i++) {
                    let continuewotch = await contents.Q_continuewatching(result[i].id,usergen);
                    if(continuewotch.length > 0){
                        arry.push()
                        arry.push({logo:result[i].logo, baner_logo:result[i].logo2, ch_name:result[i].ch_name, id:result[i].id,link:result[i].link, type:result[i].type,
                            detail:"null", current:continuewotch[0].current, duration_time:continuewotch[0].duration_time,ep_no:continuewotch[0].ep_no});
                    }else{
                        arry.push({logo:result[i].logo, baner_logo:result[i].logo2, ch_name:result[i].ch_name, id:result[i].id,link:result[i].link, type:result[i].type,
                            detail:"null", ch_num:0,current:"null", duration_time:"null",ep_no:"null"});
    
                    }
                }
                resolve(arry);

            }
        });
    });
};
contents.Q_subtabfrommainpage = function(id_mainpage){
    return new Promise((resolve, reject) => {
        sql.query("SELECT subtab.id, subtab.name,subtab.subtab_type as type,subtab.img FROM subtab WHERE maintab_id = '"+id_mainpage+"' ORDER BY subtab.order_sort ASC",(err, result) => {
            if(err)throw err;
            resolve(result)
        });
    });
};
contents.Q_contentfromsubtab_live = function(id){
    return new Promise((resolve, reject) => {
        sql.query("SELECT contentdata.id,contentdata.logo1 as logo ,contentdata.logo2 ,contentdata.name_th as ch_name ,contentdata.link_full as link ,contentdata.chanel_num as ch_num FROM  maintab INNER JOIN subtab ON subtab.maintab_id = maintab.id INNER JOIN content_subtab ON content_subtab.subtab_id = subtab.id INNER JOIN contentdata ON contentdata.id = content_subtab.contentdata_id WHERE subtab.id = '"+id+"' AND contentdata.deploy_status = '1' ORDER BY content_subtab.num_sort ASC",(err, result) => {
            if(err)throw err;
            resolve(result)
        });
    });
};
contents.Q_contentfromsubtab_vod = function(id,subname,usergen,type){
    return new Promise((resolve, reject) => {
        var datacontent = {status:"0",logo:"",baner_logo:"",ch_name:"",id:"",types:"",detail:"",current:"",duration_time:"",ep_no:""} 
        var arry = []
        sql.query("SELECT contentdata.logo1 as logo,contentdata.logo2 as baner_logo,contentdata.name_th as ch_name,contentdata.id,mediadata.name as type ,contentdata.detail FROM maintab INNER JOIN subtab ON subtab.maintab_id = maintab.id INNER JOIN content_subtab ON content_subtab.subtab_id = subtab.id INNER JOIN contentdata ON contentdata.id = content_subtab.contentdata_id INNER JOIN mediadata ON mediadata.id = contentdata.mediadata_id WHERE subtab.id = '"+id+"' AND contentdata.deploy_status = '1'",async(err, result) => {
            if(err)throw err;
            if(result.length > 0){
                for (let i = 0; i < result.length; i++) {
                    let continuewotch = await contents.Q_continuewatching(result[i].id,usergen);
                    if(continuewotch.length > 0){
                        arry.push()
                        arry.push({logo:result[i].logo, baner_logo:result[i].baner_logo, ch_name:result[i].ch_name, id:result[i].id, type:result[i].type,
                            detail:result[i].detail, current:continuewotch[0].current, duration_time:continuewotch[0].duration_time,ep_no:continuewotch[0].ep_no});
                    }else{
                        arry.push({logo:result[i].logo, baner_logo:result[i].baner_logo, ch_name:result[i].ch_name, id:result[i].id, type:result[i].type,
                            detail:result[i].detail, current:"null", duration_time:"null",ep_no:"null"});

                    }
                }
                resolve(arry);
            }else{
                if(subname == "Continue Watching"){
                    sql.query("SELECT contentdata.logo1 as logo,contentdata.logo2 as baner_logo,contentdata.name_th as ch_name,contentdata.id,mediadata.name as type ,contentdata.detail , continue_watching.current , continue_watching.duration_time , continue_watching.ep_no  FROM contentdata INNER JOIN mediadata ON mediadata.id = contentdata.mediadata_id INNER JOIN continue_watching ON continue_watching.contentdata_id = contentdata.id WHERE continue_watching.username = '"+usergen+"'AND mediadata.name = '"+type+"' AND contentdata.deploy_status = '1'",(err, resultctn) => {
                        if(err)throw err;
                        if(resultctn.length > 0){
                            resolve(resultctn);
                        }else{
                        resolve(arry);
                        }
                    });
                }else{
                    resolve(arry);
                }
            }
        });
    });
};
contents.Q_continuewatching = function(convod,usergen){
    return new Promise((resolve, reject) => {
        sql.query("SELECT * FROM continue_watching WHERE username = '"+usergen+"' AND contentdata_id = '"+convod+"'",(err, result) => {
            if(err)throw err;
            resolve (result);
        });
    });
};
contents.Q_content = function(idcontent,usergen){
    return new Promise((resolve, reject) => {
        var arry = []
        sql.query("SELECT contentdata.ch_name, contentdata.logo1 as logo,contentdata.logo2,contentdata.name_th as nameth, contentdata.name_en as nameen ,contentdata.allstars,contentdata.directors,contentdata.detail,mediadata.name as type ,contentdata.link_preview as preview,contentdata.link_full as link  FROM contentdata INNER JOIN mediadata ON mediadata.id = contentdata.mediadata_id WHERE contentdata.id = '"+idcontent+"' AND contentdata.deploy_status = '1'",(err, result) => {
            if(err)throw err;
            sql.query("SELECT * FROM continue_watching WHERE username = '"+usergen+"' AND contentdata_id = '"+idcontent+"'",(err, resultA) => {
                if(err)throw err;
                if(resultA.length > 0){
                    arry.push({ch_name:result[0].ch_name, logo:result[0].logo, logo2:result[0].logo2, nameth:result[0].nameth, nameen:result[0].nameen,
                        allstars:result[0].allstars, director:result[0].directors, detail:result[0].detail,type:result[0].type,current:resultA[0].current,duration_time:resultA[0].duration_time,continue_ep:resultA[0].ep_no,preview:result[0].preview,link:result[0].link});
                    resolve(arry)
                }else{
                    arry.push({ch_name:result[0].ch_name, logo:result[0].logo, logo2:result[0].logo2, nameth:result[0].nameth, nameen:result[0].nameen,
                        allstars:result[0].allstars, director:result[0].directors, detail:result[0].detail,type:result[0].type,current:"null",duration_time:"null",continue_ep:"null",preview:result[0].preview,link:result[0].link});
                    resolve(arry)
                }
            });
        });
    });
};
contents.Q_ep = function(idcontent){
    return new Promise((resolve, reject) => {
        sql.query("SELECT episode.ep_no,episode.ep_name,episode.ep_link FROM contentdata INNER JOIN episode ON episode.contentdata_id = contentdata.id WHERE contentdata.id = '"+idcontent+"' AND episode.deploy_status = '1'",(err, result) => {
            if(err)throw err;
            resolve(result)
        });
    });
};
contents.Q_contentname = function(namecharnal){
    return new Promise((resolve, reject) => {
        sql.query("SELECT contentdata.id FROM contentdata WHERE ch_name = '"+namecharnal+"'",(err, result) => {
            if(err)throw err;
            resolve(result[0].id);
        });
    });
};

contents.test = function (){
    
    return new Promise((resolve, reject) => {
        sql.query("",(err, result) => {
            if(err)throw err;
        });
    });

};
module.exports = contents;