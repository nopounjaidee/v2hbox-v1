var sql = require('../connects/con_v2hdb');
var service = {};
service.gencode = function (length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * charactersLength)
      );
    }
    return result;
  };
service.gencodenum = function (length) {
    var result = "";
    var characters = "0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * charactersLength)
      );
    }
    return result;
  };
service.plustime = function (length) {
    var nows = new Date();
    // nows.setMinutes(nows.getMinutes() + length); // + x นาที 
    nows.setHours(nows.getHours() + length); // + x นาที 
    now = new Date(nows); // Date object
    var day = String(now.getDate()).padStart(2, "0");
    var month = String(now.getMonth() + 1).padStart(2, "0");
    var expirydate = now.getFullYear() + "/" + month + "/" + day + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    // console.log("fn plustime" + expirydate );
    return expirydate;
  };
service.plusyear = function (length) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0");
    var yyyy = today.getFullYear() + length;
    var times = today.toTimeString();
    var expirydate = yyyy + "/" + mm + "/" + dd + " " + times.split(" ")[0];
    return expirydate;
  };
service.q_maintenance = function (data, callback) {
    // var { prj, seapp, pbip, syss, chi } = data;
    sql.query("SELECT * FROM maintenance_v2hbox WHERE (now() >= start_time and now() <= end_time) and status = 1", (err, result) => {
        if (err) throw err ;
        var res_back = {
            maintenance : "",
            massage:""
        }
        if (result.length > 0) {
            res_back.massage = result[0].massage
            res_back.maintenance = result[0].status
        }else{
            res_back.massage = "null"
            res_back.maintenance = "0"
        }

        callback(null,res_back);
    });
  
  };
service.ma_keycode = async function (data,callback){
  var {mkc,qt} = data
  var calldata = []
  for(let counti = 0;counti < qt; counti++){
     do {
        var keycodess = await service.genkeycodes(mkc);
        var {status,key1,key2} = keycodess
        console.log("status : "+status)
    } while (status);
    console.log(counti + "---"+key1+"-"+key2 );
    sql.query("INSERT INTO `keycode_v2hbox` (keycode1,keycode2,create_date) VALUES ('"+key1+"','"+key2+"',NOW());", async (err, result) => {
      if (err) throw err ;
    });
    calldata.push(key1)
  }
  console.log("END LOOP :"+calldata)
  callback(null,calldata);
};
service.genkeycodes = function (mkc){
  return new Promise((resolve, reject) => {
    let code1 =  service.gencodenum(4)
    let code2 =  service.gencodenum(4)
    let code3 =  service.gencodenum(4)
    var key1 = mkc+"-"+code1+"-"+code2+"-"+code3
    var key2 = mkc+code1+code2+code3
    var resdata = {status:false,key1:"",key2:""}
    console.log("key"+key1+"-"+key2 );
    sql.query("SELECT * FROM `keycode_v2hbox` WHERE keycode1 = '"+key1+"' and keycode2 ='"+key2+"'", async (err, result) => {
        if (err) throw err ;
        if(result.length > 0){
          resdata.status = true
          resdata.key1 = "null"
          resdata.key2 = "null"
          resolve(resdata);
        }else{
          resdata.status = false
          resdata.key1 = key1
          resdata.key2 = key2
          resolve(resdata);
        }
    });
  });
};
service.checkversion_box = function (versionnum,callback){
  var datares = {version:"",link:"",status:"",massage:""}
  sql.query("SELECT appversion as version ,link,status FROM `version_app` WHERE status = 1", (err, result) => {
    if (err) throw err ;
    if(result.length > 0 ){
      if(versionnum < result[0].version){
           datares.status = "1"
           datares.version = result[0].version ;
           datares.link = result[0].link ;
           datares.massage = "version bypass" ;
           callback(null,datares);
      }else{
            datares.status = "0"
            datares.version = "null";
            datares.link = "null" ;
            datares.massage = "non updateversion" ;
            callback(null,datares);
      }
      
    }else{
      sql.query("SELECT appversion as version ,link,status FROM `version_app` WHERE appversion = (SELECT MAX(appversion) FROM version_app WHERE NOT `status` = 2)",  (err, result) => {
        if (err) throw err ;
        if(result.length > 0 ){
          if(versionnum < result[0].version){
            datares.status = "1"
            datares.version = result[0].version ;
            datares.link = result[0].link ;
            datares.massage = "update to last version" ;
            callback(null,datares);
          }else{
             datares.status = "0"
             datares.version = "null";
             datares.link = "null" ;
             datares.massage = "non updateversion" ;
             callback(null,datares);
          }

        }else{
              datares.status = "0"
             datares.version = "null";
             datares.link = "null" ;
             datares.massage = "มีบางอย่างผิดพลาด!!" ;
             callback(null,datares);
        }
      });
    }
  });
  
};

  module.exports = service;