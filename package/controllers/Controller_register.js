var sql = require('../connects/con_v2hdb');
var Services = require('../controllers/Controller_service');
var register = {};
register.checkuserbox = function(usergen,callback){
    let data = {status:"",massess:"",usergen:""}
    sql.query("SELECT * FROM `user_v2hbox` WHERE usergen = '" + usergen +"' AND NOW() < expiry", (err, result) => {
        if (err) throw err ;
        if (result.length > 0) {//user ยังไม่หมดอายุการใช้งาน
            data.status = "1"
            data.massess = "ทำรายการสำเร็จ"
            data.usergen = usergen
            callback(null,data)
        }else{//user หมดอายุการใช้งาน
            data.status = "0"
            data.massess = "Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264"
            data.usergen = usergen
            callback(null,data)
        }
      });
};
register.checkuserdatabox = function (databox,callback){
    var {maclan,macwifi,sn} = databox;
    let data = {status:"",massess:"",usergen:""}
    sql.query("SELECT user_v2hbox.usergen  FROM user_v2hbox INNER JOIN v2hbox_data ON v2hbox_data.id = user_v2hbox.v2hbox_data_id WHERE v2hbox_data.mac_lan LIKE '%"+maclan+"%' AND v2hbox_data.mac_wifi LIKE '%"+macwifi+"%' AND v2hbox_data.serial LIKE '%"+sn+"%'", (err, result) => {
        if(err)throw err;
        if (result.length > 0) {
        sql.query("SELECT * FROM `user_v2hbox` WHERE usergen = '"+result[0].usergen+"' AND NOW() < expiry",(err, result) => {
            if(err)throw err;
            if (result.length > 0) {
                    data.status = "1"
                    data.massess = "ทำรายการสำเร็จ"
                    data.usergen = result[0].usergen
                    callback(null,data)

            }else{
                    data.status = "0"
                    data.massess = "Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264"
                    data.usergen = "null"
                    callback(null,data)
            }
        });
        }else{
            data.status = "0"
            data.massess = "ยังไม่มีการลงทะเบียน"
            data.usergen = "null"
            callback(null,data)
        }
    });
};
register.authenboxtv = async function(databox,callback){
let {keycode_regist} = databox
let datares = {status:"",massege:"",user:""}
  let ch_keycode = await register.checkkeycode_authen(keycode_regist);
  if(ch_keycode.status == "1"){
      let ch_data_insert = await register.chdata_insert_authen(databox);
      datares.status = ch_data_insert.status  
      datares.massege = ch_data_insert.massege  
      datares.user = ch_data_insert.usergen  
      callback(null,datares);
      
  }else{
    datares.status = ch_keycode.status  
    datares.massege = ch_keycode.massege  
    datares.user = "null"  
    callback(null,datares);
  }
};
register.checkkeycode_authen = function(keycode){
  return new Promise((resolve, reject) => {
      var datares = {status:"",massege:""}
    sql.query("SELECT * FROM `keycode_v2hbox` WHERE keycode1 = '" + keycode + "'",(err, result) => {
        if(err)throw err;
        if(result.length > 0){//Keycode ถูกต้อง
            sql.query("SELECT keycode1 FROM v2hbox_data WHERE keycode1 = '" + keycode + "'",(err, result) => {
                if(err)throw err;
                if(result.length > 0){//Keycode ถูกใช้งานไปแล้ว
                    datares.status = "0"
                    datares.massege = "keycode ถูกใช้งานไปแล้ว"
                    resolve(datares);
                }else{//Keycode ยังถูกใช้งาน
                    sql.query("SELECT v2hbox_data.id as databox_id ,keycode_v2hbox.id as keycode_id, v2hbox_data.keycode1   FROM v2hbox_data  INNER JOIN keycode_v2hbox ON   v2hbox_data.keycode1 = keycode_v2hbox.keycode1 INNER JOIN user_v2hbox ON user_v2hbox.v2hbox_data_id = v2hbox_data.id  WHERE  v2hbox_data.keycode1 = '" + keycode + "'",(err, result) => {
                        if(err)throw err;
                        if(result.length > 0){//Keycode ถูกใช้งานไปแล้ว
                            datares.status = "0"
                            datares.massege = "keycode ถูกใช้งานไปแล้ว"
                            resolve(datares);
                        }else{//Keycode ยังถูกใช้งาน
                            sql.query("SELECT * FROM `keycode_v2hbox` WHERE keycode1 ='" + keycode + "' AND now() > start_expiry",(err, result) => {
                                if(err)throw err;
                                if(result.length > 0){//Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264
                                    datares.status = "0"
                                    datares.massege = "Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264"
                                    resolve(datares);
                                }else{//Keycode สามารถ Authenได้
                                    datares.status = "1"
                                    datares.massege = "Keycode สามารถ Authenได้"
                                    resolve(datares);                                    
                                }
                            });
                        }
                    });
                }
            });
        }else{//Keycode ไม่ถูกต้อง
            datares.status = "0"
            datares.massege = "Keycode ไม่ถูกต้องกรุณา ลองใหม่อีกครั่งค่ะ "
            resolve(datares);
        }
    });
  });
};
register.chdata_insert_authen = function(databox){
    let { mac_lan_regist,mac_wifi_regist,serial_regist,
        release_regist,incremental_regist,sdk_int_regist ,
        board_regist,bootloader_regist , brand_regist ,
        cpu_abi_regist,cpu_abi2_regist ,device_regist ,
        display_regist ,fingerprint_regist , hardware_regist ,
        host_regist ,id_regist ,manufacturer_regist,
        model_regist ,product_regist,tags_regist,
        time_regist ,type_regist,unknown_regist ,
        user_regist ,ipaddress_regist , version_app_regist ,
        keycode_regist 
       } = databox
       return new Promise((resolve, reject) => {
        var datares = {status:"",massege:"",usergen:""}
       sql.query("SELECT user_v2hbox.usergen , user_v2hbox.v2hbox_data_id  FROM user_v2hbox INNER JOIN v2hbox_data ON v2hbox_data.id = user_v2hbox.v2hbox_data_id WHERE v2hbox_data.mac_lan = '" + mac_lan_regist + "' AND v2hbox_data.mac_wifi ='" + mac_wifi_regist + "' AND v2hbox_data.serial = '" + serial_regist + "'",(err, resultA) => {
            if(err)throw err;
            if (resultA.length > 0) {//มีการลงทะเบียนเเล้ว
                sql.query("SELECT * FROM `user_v2hbox` WHERE usergen = '" + resultA[0].usergen + "' AND NOW() < expiry",(err, resultB) => {
                    if(err)throw err;
                    if (resultB.length > 0) {//user ยังไม่หมดอายุ
                        datares.status = "1"
                        datares.massege = "ผ่านการลงทะเบียนแล้ว exp:"+resultB[0].expiry + "  keycode :"+keycode_regist+"  จะยังไม่ถูกใช้งาน"
                        datares.usergen = resultA[0].usergen
                        resolve(datares);
                    }else{//user หมดอายุ
                        // ทำการอัพเดท keycode
                        sql.query("UPDATE v2hbox_data SET keycode1 = '"+keycode_regist+"' WHERE id = '"+resultA[0].v2hbox_data_id+"'",(err, resultC) => {
                            if(err)throw err;
                            // Get ID Keycode to update User_v2hbox
                            sql.query("SELECT v2hbox_data.id as databox_id ,keycode_v2hbox.id as keycode_id, v2hbox_data.keycode1   FROM v2hbox_data  INNER JOIN keycode_v2hbox ON  v2hbox_data.keycode1 = keycode_v2hbox.keycode1 WHERE  v2hbox_data.keycode1 = '" + keycode_regist + "'",(err, resultD) => {
                                if(err)throw err;
                                if(resultD.length > 0){
                                    // update User_v2hbox
                                    sql.query("UPDATE user_v2hbox SET keycode_v2hbox_id = '"+resultD[0].keycode_id+"' WHERE usergen = '"+resultA[0].usergen+"'",(err, resultE) => {
                                        if(err)throw err;
                                        // get all data insert to user_v2hbox_history
                                        sql.query("SELECT*FROM user_v2hbox  INNER JOIN keycode_v2hbox ON  user_v2hbox.keycode_v2hbox_id = keycode_v2hbox.id  WHERE  keycode_v2hbox.keycode1 = '" + keycode_regist + "'",(err, resultF) => {
                                            if(err)throw err;
                                            if(resultF.length > 0){
                                                //  insert to user_v2hbox_history
                                                sql.query("INSERT INTO `user_v2hbox_history`(user_v2hbox_id,keycode_id,activekey) VALUES ('" + resultF[0].v2hbox_data_id + "','" + resultF[0].keycode_v2hbox_id + "',NOW());",(err, resultG) => {
                                                    if(err)throw err;
                                                    datares.status = "1"
                                                    datares.massege = "Updatecode ทำรายการสำเร็จ"
                                                    datares.usergen = resultA[0].usergen
                                                    resolve(datares);
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            }else{//ยังไม่มีการลงทะเบียน
                let insert_boxdata = "INSERT INTO `v2hbox_data` (mac_lan,mac_wifi,serial,releases,incremental,sdk_int,board,bootloader,brand,cpu_abi,cpu_abi2,device,display,fingerprint,hardware,host,v2hbox_id,manufacturer,model,product,tags,time,type,unknown,user,ipaddress,version_app,keycode1,keycode2)"
                                        + " VALUES ('" + mac_lan_regist + "','" + mac_wifi_regist + "','" + serial_regist + "','" + release_regist + "','" + incremental_regist + "','" + sdk_int_regist + "','" + board_regist + "','" + bootloader_regist + "','" + brand_regist + "','" + cpu_abi_regist + "','" + cpu_abi2_regist + "','" + device_regist + "','" + display_regist + "','" + fingerprint_regist + "','" + hardware_regist + "','" + host_regist + "','" + id_regist + "','" + manufacturer_regist + "','" + model_regist + "','" + product_regist + "','" + tags_regist + "','" + time_regist + "','" + type_regist + "','" + unknown_regist + "','" + user_regist + "','" + ipaddress_regist + "','" + version_app_regist + "','" + keycode_regist + "','');";
                sql.query(insert_boxdata,(err, resultAA) => {
                     if(err)throw err;
                     sql.query("SELECT v2hbox_data.id as databox_id ,keycode_v2hbox.id as keycode_id, v2hbox_data.keycode1   FROM v2hbox_data  INNER JOIN keycode_v2hbox ON  v2hbox_data.keycode1 = keycode_v2hbox.keycode1 WHERE  v2hbox_data.keycode1 = '" + keycode_regist + "'",(err, resultBB) => {
                        if(err)throw err;
                        if(resultBB.length > 0){
                            let slice_maclan_befor = mac_lan_regist.slice(0, 2);
                            let slice_maclan_after = mac_lan_regist.slice(-2);
                            let usergens = slice_maclan_befor + Services.gencode(4) + slice_maclan_after;
                            let insert_usergen = "INSERT INTO `user_v2hbox`(usergen,v2hbox_data_id,keycode_v2hbox_id,expiry)"
                                + "VALUES ('" + usergens + "','" + resultBB[0].databox_id + "','" + resultBB[0].keycode_id + "','" + Services.plusyear(2) + "');";
                            sql.query(insert_usergen,(err, resultCC) => {
                               if(err)throw err;
                               sql.query("SELECT*FROM user_v2hbox  INNER JOIN keycode_v2hbox ON  user_v2hbox.keycode_v2hbox_id = keycode_v2hbox.id  WHERE  keycode_v2hbox.keycode1 = '" + keycode_regist + "'",(err, resultDD) => {
                                  if(err)throw err;
                                  if(resultDD.length > 0){
                                    let insert_v2hbox_history = "INSERT INTO `user_v2hbox_history`(user_v2hbox_id,keycode_id,activekey)"
                                        + "VALUES ('" + resultDD[0].v2hbox_data_id + "','" + resultDD[0].keycode_v2hbox_id + "',NOW());";
                                       sql.query(insert_v2hbox_history,(err, resultEE) => {
                                          if(err)throw err;
                                          datares.status = "1"
                                          datares.massege = "ทำรายการสำเร็จ"
                                          datares.usergen = usergens
                                          resolve(datares);
                                      });
                                  }
                              });
                           });
                        }
                    });
                });
            }
        });
    });
};
module.exports = register;