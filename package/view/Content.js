var sql = require('../connects/con_v2hdb');
var Contents = require('../controllers/Controller_content');
module.exports = {
    Memumain:async (req,res)=>{
        let usergen = req.body.usergen;
        let callback = await Contents.Q_mainpagemenu();
        res.json({ status: "1",message:"MainTab", menu: callback});

    },
    Livetvall:async(req,res)=>{
        let usergen = req.body.usergen;
        let datares = await Contents.Q_livetvall();
        res.json({ status: "1",subtitle:"recommend", body: datares});
    },
    Contentdata:async(req,res)=>{
        let id_mainpage = req.body.id
        let usergen = req.body.usergen
        var type = "live";
        if(id_mainpage == "2"){
            type = "movie"
        }else if (id_mainpage == "3"){
            type = "serie"
        }else if(id_mainpage == "4"){
            type = "varity"
        }

        let data_contentdata = await Contents.Q_contentdata(id_mainpage,usergen,type);
        res.json({status:"1",message:type,body:data_contentdata});

    },
    Contentdatasubtab:async(req,res)=>{ //ส่ง ID subtab มาขอ details Content (กรณีนี้ tab see all)
        let idsubtab = req.body.idsubtab
        let usergen = req.body.usergen
        let data_contentdatasubtab = await Contents.Q_contentsubtab(idsubtab,usergen);
        res.json({item:data_contentdatasubtab})
    },
    Content:async(req,res)=>{ //ส่ง ID Content มาขอ link player 
        let id_content = req.body.id
        let usergen = req.body.usergen
        let type_content = req.body.type
        switch (type_content) {
            case "movie":
                    let datamove = await Contents.Q_content(id_content,usergen);
                    let link = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+datamove[0].ch_name+"/"+datamove[0].link+"?type=dash"
                    res.json({ ch_name:datamove[0].ch_name,logo:datamove[0].logo,logo2:datamove[0].logo2,nameth:datamove[0].nameth,nameen:datamove[0].nameen,allstars:datamove[0].allstars,director:datamove[0].directors,detail:datamove[0].detail,type:datamove[0].type,current:datamove[0].current,duration_time:datamove[0].duration_time,continue_ep:datamove[0].continue_ep,preview:datamove[0].preview,link:link});
                break;
            case "serie":
                let dataserie = await Contents.Q_content(id_content,usergen);
                let epserie = await Contents.Q_ep(id_content);
                let ep_serie = []
                for(let i = 0;i < epserie.length;i++){
                    var eplink = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+dataserie[0].ch_name+"/"+epserie[i].ep_link+"?type=dash"
                    var arreps = {ep_no:epserie[i].ep_no,ep_name:epserie[i].ep_name,ep_link:eplink}
                    ep_serie.push(arreps);
                }
                res.json({ ch_name:dataserie[0].ch_name,logo:dataserie[0].logo,logo2:dataserie[0].logo2,nameth:dataserie[0].nameth,nameen:dataserie[0].nameen,allstars:dataserie[0].allstars,director:dataserie[0].directors,detail:dataserie[0].detail,type:dataserie[0].type,current:dataserie[0].current,duration_time:dataserie[0].duration_time,continue_ep:dataserie[0].continue_ep,preview:dataserie[0].preview,episode:ep_serie});
                break;
            case "varity":
                    let datavarity = await Contents.Q_content();
                    let epvarity = await Contents.Q_ep(id_content);
                    let ep_varity = []
                    for(let i = 0;i < epvarity.length;i++){
                        var eplink = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+datavarity[0].ch_name+"/"+epvarity[i].ep_link+"?type=dash"
                        var arreps = {ep_no:epvarity[i].ep_no,ep_name:epvarity[i].ep_name,ep_link:eplink}
                        ep_varity.push(arreps);
                    }
                    res.json({ ch_name:datavarity[0].ch_name,logo:datavarity[0].logo,logo2:datavarity[0].logo2,nameth:datavarity[0].nameth,nameen:datavarity[0].nameen,allstars:datavarity[0].allstars,director:datavarity[0].directors,detail:datavarity[0].detail,type:datavarity[0].type,current:datavarity[0].current,duration_time:datavarity[0].duration_time,continue_ep:datavarity[0].continue_ep,preview:datavarity[0].preview,episode:ep_varity});
                break;
            default:
                break;
        }
    },
    Continuewatching:async(req,res)=>{
        let usergen = req.body.usergen;
        let content_id = req.body.content_id;
        let ep_no = req.body.ep_no;
        let type = req.body.type;
        let current = req.body.current;
        let duration_time = req.body.duration_time;

        sql.query("SELECT * FROM continue_watching WHERE username = '"+usergen+"' AND contentdata_id = '"+content_id+"'", (err, resultA) => {
            if (err) throw err;
            if(resultA.length > 0){
                sql.query("UPDATE continue_watching SET ep_no='"+ep_no+"',current='"+current+"',duration_time='"+duration_time+"',update_at= NOW() WHERE id = '"+resultA[0].id+"'", (err, result) => {
                    if (err) throw err;
                    // console.log('Update : ');
                    res.json({"status":"1",message:"Update success"});
                });
            }else{
                sql.query("INSERT INTO continue_watching( username, contentdata_id, ep_no, type, current, duration_time,update_at) VALUES ('"+usergen+"','"+content_id+"','"+ep_no+"','"+type+"','"+current+"','"+duration_time+"',NOW())", (err, result) => {
                    if (err) throw err;
                    // console.log('Insert : ');
                    res.json({"status":"1",message:"Insert success"});
                });
            }
        });
    },
    ContenpluslinkA:async(req,res)=>{
        let content_id = req.body.id;
        let content_type = req.body.type;
        let contentlink = await Contents.Q_content(content_id,"");
        switch (content_type) {
            case "1":
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:[contentlink[0].link]});
                break;
            case "2":
                    let linkm = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+contentlink[0].ch_name+"/"+contentlink[0].link+"?type=dash"
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:[linkm]});
                break;
            case "3":
                    let eplinkse = await Contents.Q_ep(content_id);
                    var eps = [];
                    for (let i = 0; i < eplinkse.length; i++) {
                        let eplink = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+contentlink[0].ch_name+"/"+eplinkse[i].ep_link+"?type=dash"
                        eps.push(eplink);
                    }
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:eps});
                break;
            case "4":
                    let eplink = await Contents.Q_ep(content_id);
                    var eps = [];
                    for (let i = 0; i < eplink.length; i++) {
                        var eplinks = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+contentlink[0].ch_name+"/"+eplink[i].ep_link+"?type=dash"
                        eps.push(eplinks);
                    }
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:eps});
                break;
            default:
                //type ไม่ถูก
                break;
        }

    },
    ContenpluslinkB:async(req,res)=>{
        var chname = req.params.chname
        var type = req.params.type
        let idcontent = await Contents.Q_contentname(chname);
        let contentlink = await Contents.Q_content(idcontent,"");
        switch (type) {
            case "live":
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:[contentlink[0].link]});
                break;
            case "movie":
                    let linkm = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+contentlink[0].ch_name+"/"+contentlink[0].link+"?type=dash"
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:[linkm]});
                break;
            case "serie":
                    let eplinkse = await Contents.Q_ep(idcontent);
                    var eps = [];
                    for (let i = 0; i < eplinkse.length; i++) {
                        let eplink = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+contentlink[0].ch_name+"/"+eplinkse[i].ep_link+"?type=dash"
                        eps.push(eplink);
                    }
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:eps});
                break;
            case "varity":
                    let eplink = await Contents.Q_ep(idcontent);
                    var eps = [];
                    for (let i = 0; i < eplink.length; i++) {
                        var eplinks = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+contentlink[0].ch_name+"/"+eplink[i].ep_link+"?type=dash"
                        eps.push(eplinks);
                    }
                    res.json({ch_name:contentlink[0].ch_name,img:contentlink[0].logo,link:eps});
                break;
            default:
                //type ไม่ถูก
                break;
        }
    }
};