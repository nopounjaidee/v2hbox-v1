var Service = require('../controllers/Controller_service');
module.exports = {
    Maintenance: (req, res)=>{
        Service.q_maintenance("",(err,callback)=>{
            if (err) throw err ;
            var {maintenance,massage} = callback
            res.json({check_maintenance:maintenance,massage:massage});
        });
    },
    Createkeycode: (req, res)=>{
        let marketing_code = req.body.marketingcode;
        let quantity_code = req.body.quantity;
        var data = {mkc:marketing_code,qt:quantity_code}
        Service.ma_keycode(data,(err,callback)=>{
            if (err) throw err ;
            res.json({status:"1",keycode:callback})
        });
    },
    Checkversion: (req,res)=>{
        let versioncode = req.body.versioncode;
        Service.checkversion_box(versioncode,(err,callback)=>{
            if (err) throw err ;
            var {version,link,status,massage} = callback
            if(status == "1"){
                res.json({status:"1",massage:massage, "response": {version:version,link:link}});
            }else{
                res.json({status:"0",massage:massage, "response": {version:version,link:link}});
            }

        });
    },
};