var Registers = require('../controllers/Controller_register');
module.exports = {
    Checkuser: (req, res)=>{
        let starus_regist = req.body.status;
        let usergen_regist = req.body.usergen;
        var data = {maclan:req.body.maclan,macwifi:req.body.macwifi,sn:req.body.serialnumber}
        if (starus_regist == "1") {//กรณีที่มี user อยู่ใน ระบบ
            Registers.checkuserbox(usergen_regist,(err,callback)=>{
                if(err) throw err;
                let {status,massess,usergen} = callback;
                if(status == "1"){
                    res.json({status: "1",massess: massess, usergen: usergen});
                }else{
                    res.json({status: "0",massess: massess, usergen: usergen});
                }

            });
        }else{// กรณีที่ไม่มี User 
            Registers.checkuserdatabox(data,(err,callback)=>{
                    if(err) throw err ;
                    let {status,massess,usergen} = callback
                    if(status == "1"){
                        res.json({status: "1",massess: massess, usergen: usergen});
                    }else{
                        res.json({status: "0",massess: massess, usergen: usergen});
                    }
            });
        }
    },
    Registerboxtv: (req,res)=>{
        let databox = { mac_lan_regist : req.body.mac_lan,mac_wifi_regist : req.body.mac_wifi,serial_regist : req.body.serial,
            release_regist : req.body.release,incremental_regist : req.body.incremental,sdk_int_regist : req.body.sdk_int,
            board_regist : req.body.board,bootloader_regist : req.body.bootloader, brand_regist : req.body.brand,
            cpu_abi_regist : req.body.cpu_abi,cpu_abi2_regist : req.body.cpu_abi2,device_regist : req.body.device,
            display_regist : req.body.display,fingerprint_regist : req.body.fingerprint, hardware_regist : req.body.hardware,
            host_regist : req.body.host,id_regist : req.body.id,manufacturer_regist : req.body.manufacturer,
            model_regist : req.body.model,product_regist : req.body.product,tags_regist : req.body.tags,
            time_regist : req.body.time,type_regist : req.body.type,unknown_regist : req.body.unknown,
            user_regist : req.body.user,ipaddress_regist : req.body.ipaddress, version_app_regist : req.body.version_app,
            keycode_regist : req.body.keycode
           }
        
        if (req.body.mac_lan == "" && req.body.serial == "" && req.body.keycode == "") {
            //ไม่มีข้อมูล เข้ามา
            console.log("ไม่มีข้อมูล mac_lan , serial , key");
            res.json({massage:"ไม่มีข้อมูล mac_lan , serial , keycode"});
          } else {
                Registers.authenboxtv(databox,(err,callback)=>{
                   let {status,massege,user} = callback
                    res.json({ status:status,massess: massege,usergen:user });
                });
          }


    },
};